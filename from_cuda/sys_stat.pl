#!/usr/bin/perl

use strict;
use warnings;

my $test_website='www.google.com';

sub ConfigRead {
	my $match=$_[0];
	my @lines;

	open(CMD,"config_read $match 2>/dev/null|") || die("Can't run config_read\n");
	while(my $line=<CMD>) {
		chomp($line);
		if($line=~/$match/) {
			push(@lines,$line);
		}
	}
	close(CMD);

	return(\@lines);
}

sub GetSingleVar {
	my $var=$_[0];

	my $blah=ConfigRead($var);

	foreach my $line (@$blah) {
		my @cols=split(/\|/,$line);
		if(defined($cols[3]) && defined($cols[4]) && $cols[3]=~/^\s*$var\s*$/) {
			$cols[4]=~s/^\s*(\S+.*\S+)\s*$/$1/;
			return($cols[4]);
		}
	}

	return(undef);
}

sub CheckSQLAccess {
	my $var=GetSingleVar('system_ip');
	if(!defined($var)) {
		die("Couldn't get system IP, check mysql status\n");
	}
}

sub CheckProxyAuth {
	my $var=GetSingleVar('proxy_auth_forced');
	if(defined($var)) {
		printf("Forced Proxy Auth : %s\n",$var);
	} else {
		printf("Error retrieving proxy_auth_forced info\n");
	}
}

sub CheckTransProxy {
	my $var=GetSingleVar('spy_tproxy_enable');
	if(defined($var)) {
		printf("Transparent Proxy : %s\n",$var);
	} else {
		printf("Error retrieving spy_tproxy_enable info\n");
	}
}

sub CheckDNS {
	my $dnscache=GetSingleVar('dns_cache');
	my $dnslocal=GetSingleVar('dns_force_local');
	my $pridns=GetSingleVar('system_primary_dns_server');
	my $secdns=GetSingleVar('system_secondary_dns_server');

	if(defined($dnscache)) {
		printf("DNS Cache : %s\n",$dnscache);
	} else {
		printf("Error retrieving dns_cache info\n");
	}

	if(defined($dnslocal)) {
		printf("DNS Force Local : %s\n",$dnslocal);
	} else {
		printf("Error retrieving dns_force_local info\n");
	}

	if(!defined($pridns)) {
		printf("Primary DNS not defined\n");
	} else {
		my $querytime=DoDNSQuery($pridns,$test_website);
		if(defined($querytime)) {
			printf("Pri DNS [%s] response time : %s\n",$pridns,$querytime);
		} else {
			printf("Error with primary DNS\n");
		}
	}


	if(!defined($secdns)) {
		printf("Secondary DNS not defined\n");
	} else {
		my $querytime=DoDNSQuery($secdns,$test_website);
		if(defined($querytime)) {
			printf("Sec DNS [%s] response time : %s\n",$secdns,$querytime);
		} else {
			printf("Error with secondary DNS\n");
		}
	}
}

sub CheckDNSServer {
	my $server=$_[0];

	if($server!~/^\d+\.\d+\.\d+\.\d+$/) {
		printf("Malformed DNS entry [%s]\n",$server);
		return(undef);
	}

	return(DoDNSQuery($server,$test_website));
}

sub DoDNSQuery {
	my $server=$_[0];
	my $query=$_[1];

	open(CMD,"dig \@$server $query 2>&1|") || die("Can't run dig\n");
	while(my $line=<CMD>) {
		if($line=~/Query time:\s+(.*)/) {
			return($1);
		}
	}
	close(CMD);

	return(undef);
}

CheckSQLAccess();
CheckProxyAuth();
CheckTransProxy();
CheckDNS();
