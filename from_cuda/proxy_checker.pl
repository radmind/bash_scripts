#!/usr/bin/perl
use strict;
use Barracuda::Environment;
use Net::DNS;
use Getopt::Std;
my %type;
my $path_home = $Barracuda::Environment::path_home;
my $path = "$path_home/cfdef/current/im/proxies.ipset";
my $resolved;
getopt("d=i=", \%type,);
die "Usage: -d <domain> -i <IP>" 
	unless($type{d} or $type{i}); 


my $ipset = "/usr/bin/sudo /home/spyware/code/firmware/current/sbin/ipset -L"; 
if ($type{d}){{
	my $res   = Net::DNS::Resolver->new;
	my $query = $res->search("$type{d}");
		foreach my $rr ($query->answer) {
		next unless $rr->type eq "A";
	$resolved = $rr->address;
	}	
}

my $FD;    
open ($FD, "-|", "$ipset") or die $!;
	while (<$FD>) {
		if (/^$resolved/) {
		print qq|$type{d} FOUND in list of known proxies\n|;
		exit;
		}
	}
}


if ($type{i}){
my $FH;
open ($FH, "-|", "$ipset") or die $!;
	while (<$FH>) {
		if (/^$type{i}/) {
		print STDERR qq|$type{i} FOUND in list of known proxies\n|;
		exit;
		}     
	}	
}
