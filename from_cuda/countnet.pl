#!/usr/bin/perl -T
# [14:58:59] <jreuter> if you want to know who is using the most sockets
# count_connections.pl modified by Shawn Bater to sort by most connections first
use strict;
use warnings;
$ENV{'PATH'} = '';
$ENV{'ENV'} = '';
my %ip_list;
my @netstat = readpipe("/bin/netstat -na\|/bin/grep -i 'estab\\|time'");
foreach(@netstat){
        chomp;
        while ($_ =~ /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(.*)([^\d])(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/g){
                my $src_ip = $1;
                my $dst_ip = $4;
                unless(($src_ip || $dst_ip) eq '127.0.0.1'){
                        $ip_list{$dst_ip} += 1;
                }
        }
}

sub DesVal {
  $ip_list{$b} <=> $ip_list{$a};
}

my $key;
foreach $key (sort DesVal (keys(%ip_list))) {
  print "  $ip_list{$key} - $key\n";
}
              