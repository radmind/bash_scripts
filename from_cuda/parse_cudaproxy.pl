#!/usr/bin/perl

print "Please enter the location of the file: \n";
my $location = <STDIN>;
chomp ($location);

my $file = $location;

# Read log
my $cudaproxy = `/usr/bin/tail -n 1 $file`;
#open (CUDAPROXY, "< $file") || die "Cannot open file: $!\n\n";
#my @cudaproxy = <CUDAPROXY>;
#close CUDAPROXY;

# Execute subroutine
#my $timestamp = get_unix_timestamp(pop @cudaproxy);
my $timestamp = get_unix_timestamp($cudaproxy);

#to check if the file is being pushed correctly
print "TIMESTAMP: $timestamp\n\n";
rename("$file","cudaproxy.log.".$timestamp);

# Subroutine
sub get_unix_timestamp() {
    my ($line, $timestamp) = (shift, ' ');
    if ($line =~ /^(?:\S+\s+){5}(\d{10})/) {
        $timestamp = $1;
    }

    return $timestamp;
}

#Dec  3 09:00:57 Barracuda http_scan[9114]: 1291395657 1 216.101.241.69 66.220.151.80 text/plain 216.101.241.69 http://0.149.channel.facebook.com/x/3809843830/954971897/true/p_503364311=3 257 BYF ALLOWED CLEAN  2 1 1 0 4 (-) 1 CUSTOM-2 0 - 0 facebook.com social-networking,CUSTOM-2 [ldap0:cwollick]  http://0.149.channel.facebook.com/iframe/11?r=http%3A%2F%2Fstatic.ak.fbcdn.net%2Frsrc.php%2Fzy%2Fr%2F1mYgyiDIRNC.js&r=http%3A%2F%2Fstatic.ak.fbcdn.net%2Frsrc.php%2FzU%2Fp%2Fr%2Fr-WcbXXN1T7.js&r=http%3A%2F%2Fstatic.ak.fbcdn.net%2Frsrc.php%2Fzl%2Fp%2Fr%2FJaKCjFxJ2wP.js&r=http%3A%2F%2Fstatic.ak.fbcdn.net%2Frsrc.php%2Fzm%2Fp%2Fr%2FEjCkrXLXn6D.js
