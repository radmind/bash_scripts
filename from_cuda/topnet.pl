#!/usr/bin/perl

use strict;
use warnings;

my $FILTER="(tcp or udp) and not port 22 and not port 80 and not port 53";

select STDERR; $| = 1;
select STDOUT; $| = 1;

my %UDPSVC;
my %TCPSVC;
my %SRC_TCP_PORTS;
my %DST_TCP_PORTS;
my %SRC_UDP_PORTS;
my %DST_UDP_PORTS;
my %SRCIPS;
my %DSTIPS;
my $TIMELIMIT;

sub LoadServices {
	my $NAME;
	my $PORT;
	my $PROTO;

	open(SVC,"/etc/services") || die("Can't load /etc/services\n");
	while(my $LINE=<SVC>) {
		chomp($LINE);
		if($LINE=~/^(\S+)\s+(\d+)\/(tcp|udp)/) {
			$NAME=$1;
			$PORT=$2;
			$PROTO=$3;

			if($PROTO eq "tcp") {
				$TCPSVC{$PORT}=$NAME;
			} elsif($PROTO eq "udp") {
				$UDPSVC{$PORT}=$NAME;
			}
		}
	}
	close(SVC);
}

sub GetSVC {
	my $PORT=$_[0];
	my $PROTO=$_[1];
	my $SERVICE;

	if($PROTO eq "tcp") {
		if(defined($TCPSVC{$PORT})) {
			$SERVICE="$PORT ($TCPSVC{$PORT})";
		} else {
			$SERVICE=$PORT;
		}
	} elsif($PROTO eq "udp") {
		if(defined($UDPSVC{$PORT})) {
			$SERVICE="$PORT ($UDPSVC{$PORT})";
		} else {
			$SERVICE=$PORT;
		}
	}

	return($SERVICE);
}

sub Capture {
	my $SRCIP;
	my $DSTIP;
	my $SRCPORT;
	my $DSTPORT;
	my $PROTO;
	my $START=time();
	my $CURRTIME;

	open(TCPD,"tcpdump -nq \"$FILTER\" 2>/dev/null|") || die("Can't run tcpdump\n");

	while(my $LINE=<TCPD>) {
		chomp($LINE);

		if($LINE=~/^[\d\.:]+\s(\d+\.\d+\.\d+\.\d+)\.(\d+)\s+>\s+(\d+\.\d+\.\d+\.\d+)\.(\d+):\s+(\S+)/) {
			$SRCIP=$1;
			$SRCPORT=$2;
			$DSTIP=$3;
			$DSTPORT=$4;
			$PROTO=$5;

			if(defined($SRCIPS{$SRCIP})) {$SRCIPS{$SRCIP}++; } else {$SRCIPS{$SRCIP}=1;}
			if(defined($DSTIPS{$DSTIP})) {$DSTIPS{$DSTIP}++; } else {$DSTIPS{$DSTIP}=1;}

			if($PROTO eq "tcp") {
				if(defined($SRC_TCP_PORTS{$SRCPORT})) {$SRC_TCP_PORTS{$SRCPORT}++; } else {$SRC_TCP_PORTS{$SRCPORT}=1;}
				if(defined($DST_TCP_PORTS{$DSTPORT})) {$DST_TCP_PORTS{$DSTPORT}++; } else {$DST_TCP_PORTS{$DSTPORT}=1;}
			} elsif($PROTO eq "udp") {
				if(defined($SRC_UDP_PORTS{$SRCPORT})) {$SRC_UDP_PORTS{$SRCPORT}++; } else {$SRC_UDP_PORTS{$SRCPORT}=1;}
				if(defined($DST_UDP_PORTS{$DSTPORT})) {$DST_UDP_PORTS{$DSTPORT}++; } else {$DST_UDP_PORTS{$DSTPORT}=1;}
			}
		}
		last if(time() > ($START+$TIMELIMIT));
	}

	close(TCPD);
}


if(defined($ARGV[0])) {
	$TIMELIMIT=$ARGV[0];
} else {
	die("Need a time limit to capture for\n");
}

LoadServices();
Capture();

my $CNT;

$CNT=1;
printf("Top source TCP ports\n");
printf("Rank Count    Port (Protocol)\n");
printf("~~~~ ~~~~~~~~ ~~~~~~~~~~~~~~~\n");
for my $x (sort {$SRC_TCP_PORTS{$b} <=> $SRC_TCP_PORTS{$a} } keys(%SRC_TCP_PORTS)) {
	printf("%4d %8d %s\n",$CNT,$SRC_TCP_PORTS{$x},GetSVC($x,"tcp"));
	last if($CNT++>=10);
}
printf("\n");

$CNT=1;
printf("Top destination TCP ports\n");
printf("Rank Count    Port (Protocol)\n");
printf("~~~~ ~~~~~~~~ ~~~~~~~~~~~~~~~\n");
for my $x (sort {$DST_TCP_PORTS{$b} <=> $DST_TCP_PORTS{$a} } keys(%DST_TCP_PORTS)) {
	printf("%4d %8d %s\n",$CNT,$DST_TCP_PORTS{$x},GetSVC($x,"tcp"));
	last if($CNT++>=10);
}
printf("\n");

$CNT=1;
printf("Top source UDP ports\n");
printf("Rank Count    Port (Protocol)\n");
printf("~~~~ ~~~~~~~~ ~~~~~~~~~~~~~~~\n");
for my $x (sort {$SRC_UDP_PORTS{$b} <=> $SRC_UDP_PORTS{$a} } keys(%SRC_UDP_PORTS)) {
	printf("%4d %8d %s\n",$CNT,$SRC_UDP_PORTS{$x},GetSVC($x,"udp"));
	last if($CNT++>=10);
}
printf("\n");

$CNT=1;
printf("Top destination UDP ports\n");
printf("Rank Count    Port (Protocol)\n");
printf("~~~~ ~~~~~~~~ ~~~~~~~~~~~~~~~\n");
for my $x (sort {$DST_UDP_PORTS{$b} <=> $DST_UDP_PORTS{$a} } keys(%DST_UDP_PORTS)) {
	printf("%4d %8d %s\n",$CNT,$DST_UDP_PORTS{$x},GetSVC($x,"udp"));
	last if($CNT++>=10);
}
