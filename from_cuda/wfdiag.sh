#!/bin/sh

# Webfilter Diagnose Script
#
# Updated, WF-specific version of Andrew Link's T4 script, adapted by Michael Phillippi
# Modified+improved by Shawn Bater 
# NOW WITH FLAIR!!?!
# Change log at: /home/sbater/scripts/yfdiagchanges.txt
# 
# Send requests/suggestions to: sbater@barracuda.com, mphillippi@barracuda.com
#
# Last update 15 Feb 2010

# Runtime arguments
ARGV=( `echo $@` )
ARGC=${#ARGV[@]}

# Usage check enable
echo ${ARGV[@]} | grep -e verbose -e "-v"> /dev/null
if [ $? = 0 ]; then VCHK="true" ; fi

echo ${ARGV[@]} | grep -e help -e "--help" -e "-h" > /dev/null
if [ $? = 0 ]; then
    echo "Help for $0".
    echo
    echo "verbose or -v runs YF Diag in verbose mode (extra output)"
    echo
    echo "mheller@barracuda.com"
    echo "mphillippi@barracuda.com"
    echo "sbater@barracuda.com"
    echo
    exit
fi

clear
RUNDATE=`date`
DDATE=`/usr/bin/ddate 2> /dev/null`
echo "YFDIAG V2 STARTED $RUNDATE"
echo "$DDATE"
echo

# Colorize
PASS=`echo -e '\E[0;32m'"\033[1mPASS\033[0m"`
INFO=`echo -e '\E[0;34m'"\033[1mINFO\033[0m"`
FAIL=`echo -e '\a\E[0;31m'"\033[1mFAIL\033[0m"`
WARN=`echo -e '\E[1;33m'"\033[1mWARN\033[0m"`

# Device Information
echo "DEVICE INFORMATION"
PRODUCT=`cat /etc/barracuda/product 2> /dev/null`
MODEL=`cat /etc/barracuda/model 2> /dev/null`
MODE=`cat /etc/barracuda/mode 2> /dev/null`
SERIAL=`cat /etc/barracuda/serial 2> /dev/null`
PLATFORM=`/home/spyware/code/firmware/current/bin/version.pl 2> /dev/null | grep platform | awk '{ print $2 }'`
TUNPASS=`/home/spyware/code/firmware/current/bin/support-tunnel login-print 2> /dev/null | grep pass | awk '{ print $2 }'`
IFACELIST=( `ifconfig | grep -e br0 -e eth | awk '{print $1}' | xargs echo` )
FIRMWARE=`/home/spyware/code/firmware/current/bin/version.pl 2> /dev/null | grep firmware | awk '{ print $2 }'`
FIRMWMAJ=`echo ${FIRMWARE:0:3} | sed -e 's/\.//g'`
if [ "$FIRMWMAJ" -lt "40" ]; then
    OPMODE=`config_read %spy_system_status% 2> /dev/null | grep spy_system_status | awk '{print $5}'`
else
        OPMODE=`config_read %spy_system_status% 2> /dev/null | grep spy_system_status | awk '{print $7}'`
fi
if [ "$VCHK" = "true" ]; then
    HOSTNAME=`hostname 2> /dev/null |awk '{print $1}'`
    echo "$INFO Hostname: $HOSTNAME"
fi
if [ "$PRODUCT" = "spyware" ] && [ "$MODE" = "spyware" ]; then
    echo "$INFO Product: Web Filter"
else
    echo "$FAIL Product: Not a Web Filter?"
fi
echo "$INFO Model: $MODEL"
if [ "$PLATFORM" = "" ]; then
    PLATFORM=0
fi
echo "$INFO Platform: $PLATFORM"
echo "$INFO Serial: $SERIAL"
if [ "$TUNPASS" = "" ]; then
    `support-tunnel --user=remote --pass=ZY63A2 --duration=40968980 login-add 2> /dev/null`
    echo "$INFO Tunnel Pass = ZY63A2"
fi
echo "$INFO Tunnel Pass: $TUNPASS"
if [ "$OPMODE" = "ACTIVE" ]; then
    echo "$INFO Operating mode: $OPMODE"
else
    echo "$WARN Operating mode: $OPMODE"
fi
echo

# Updates
echo "FIRMWARE AND DEFINITIONS"
CFDEF=`/home/spyware/code/firmware/current/bin/version.pl 2> /dev/null | grep cfdef | awk '{ print $2 }'`
VIRUSDEF=`/home/spyware/code/firmware/current/bin/version.pl 2> /dev/null | grep virusdef | awk '{ print $2 }'`
SPYDEF=`/home/spyware/code/firmware/current/bin/version.pl 2> /dev/null | grep spydef | awk '{ print $2 }'`
SUBM=`echo ${MODEL:1:1}`
UPDAVAL="0"
UPDLIST=( firmware cfdef virusdef spydef )
INT=${#UPDLIST[@]}
until [ $INT = 0 ]; do
    let INT-=1

    update.pl -c ${UPDLIST[INT]} | grep "Update available" > /dev/null
    if [ $? = 0 ]; then
    echo "$WARN Update ${UPDLIST[INT]} prz!"
        UPDAVAL="1"
    fi
done
if [ $UPDAVAL = "0" ]; then
    echo ""
    echo "$PASS No update available"
fi
echo "$INFO Firmware Ver: $FIRMWARE"
echo "$INFO Content Def Ver: $CFDEF"
echo "$INFO Virus Def Ver: $VIRUSDEF"
echo "$INFO Spyware Def Ver: $SPYDEF"
echo

# Networking
echo "NETWORKING"
## Internal and external IPs
if [ "$FIRMWMAJ" -lt "40" ]; then
        INTIP=`/home/spyware/code/firmware/current/bin/config_read system_ip 2> /dev/null | grep system_ip | awk '{ print $5 }'`
else
        INTIP=`/home/spyware/code/firmware/current/bin/config_read system_ip 2> /dev/null | grep system_ip | awk '{ print $7 }'`
fi
IPSITES=( checkip.dyndns.org www.ip-adress.com www.whatismyip.com\/automation\/n09230945.asp )
RAND=$[($RANDOM % ${#IPSITES[*]})]
SITE=${IPSITES[RAND]}
EXTIP=`curl $SITE 2> /dev/null | egrep -o -m 1 '([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}'`
if [ "$VERBOSE" = "true" ]; then
    echo "$INFO External IP's $EXTIP."
fi
echo "$INFO Internal IP: $INTIP"
echo "$INFO External IP: $EXTIP"
echo "$INFO NIC Speeds:"
if [ $MODEL -lt "610" ]; then
    ETH1=`mii-tool eth1 2> /dev/null`
    ETH2A=`mii-tool eth2 2> /dev/null`
        echo "        $ETH1"
        echo "        $ETH2A"
elif [ $MODEL -ge "610" ]; then
    ETH2B=`mii-tool eth2 2> /dev/null`
    ETH3=`mii-tool eth3 2> /dev/null`
        echo "        $ETH2B"
        echo "        $ETH3"
fi

## Collisions
INT=${#IFACELIST[@]}
until [ $INT = 0 ]; do
    let INT-=1

    COLL=`ifconfig | awk '{print $1}' | grep ${IFACELIST[INT]} -A6 | grep collisions | awk -F: '{print $2}'`
    if [ "${IFACELIST[INT]}" = "br0" ]; then
        echo "$INFO Heller don't care 'bout no collisions on br0, foo'"
    fi
    if [ "${IFACELIST[INT]}" != "br0" ]; then 
        if [ $COLL != 0 ]; then
            echo "$WARN ${IFACELIST[INT]}: $COLL collisions"
        else
            echo "$PASS ${IFACELIST[INT]}: no collisions"
        fi
    fi
done
if [ "$VCHK" = "true" ]; then
    SOCKHIGH=`cat /mail/log/specsum |grep SOCKUSAGEHIGH |awk '{print $2}'`
    echo "$INFO Highest Socket Use: $SOCKHIGH"
fi

## Client IP visibility
if [ "$FIRMWMAJ" -lt "40" ]; then
    IPVIS=`config_read %spy_tproxy_enable% 2> /dev/null | grep spy_tproxy_enable | awk '{print $5}'`
else
    IPVIS=`config_read %spy_tproxy_enable% 2> /dev/null | grep spy_tproxy_enable | awk '{print $7}'`
fi
if [ "$OPMODE" = "ACTIVE" ] && [ "$IPVIS" = "Yes" ]; then
    echo "$WARN Client IP visibility ON. Srsly."
fi
echo

# DNS
echo "DNS"
DNS1=`echo "select value from config where variable = 'system_primary_dns_server';" | mysql config 2> /dev/null | grep -v value`
DNS2=`echo "select value from config where variable = 'system_secondary_dns_server';" | mysql config 2> /dev/null | grep -v value`
TESTLIST=( google.com facebook.com yahoo.com bing.com microsoft.com go.com aol.com cnn.com )
DNSIPS=( 208.67.222.222 208.67.220.220 4.2.2.2 4.2.2.3 8.8.8.8 8.8.4.4 66.93.87.2 199.2.252.10 )
RANDNS=$[($RANDOM % ${#DNSIPS[*]})]
AUTODNS=${DNSIPS[RANDNS]}
if [ "$DNS1" != "" ]; then
    RANDOM=$[($RANDOM % ${#TESTLIST[*]})]
    DIG1=${TESTLIST[RANDOM]}
    LAG1=`dig @$DNS1 ${TESTLIST[${1}]} 2> /dev/null | grep "Query time:" | awk '{print $4}'`
    if [ "$LAG1" == "" ]; then
        echo "$FAIL Primary DNS: $DNS1 Non-Responsive. Try $AUTODNS"
    elif [ "$LAG1" -gt "2500" ]; then
        echo "$FAIL Primary DNS: $DNS1 @ $LAG1 msec (rly laggy)"
    elif [ "$LAG1" -gt "1000" ]; then
        echo "$WARN Primary DNS: $DNS1 @ $LAG1 msec (laggy)"
    elif [ "$LAG1" -lt "1000" ]; then
        echo "$PASS Primary DNS: $DNS1 @ $LAG1 msec"
    fi
else
    echo "$WARN Primary DNS not configured. Poor noob, try $AUTODNS"
fi
if [ "$VERBOSE" = "true" ]; then
    INT=${#IFACELIST[@]}
    until [ $INT = 0 ]; do
        let INT-=1

        INTIP=`ifconfig | grep ${IFACELIST[INT]} -A6 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`
        if [ "$INTIP" != "" ]; then
            echo "$INFO ${IFACELIST[INT]} Internal IP: $INTIP."
        fi
    done
fi
if [ "$DNS2" != "" ]; then
    RANDOM=$[($RANDOM % ${#TESTLIST[*]})]
    DIG2=${TESTLIST[RANDOM]}
    LAG2=`dig @$DNS2 ${TESTLIST[${1}]} 2> /dev/null | grep "Query time:" | awk '{print $4}'`
    if [ "$LAG2" == "" ]; then
        echo "$FAIL Secondary DNS: $DNS2 Non-Responsive, try $AUTODNS"
    elif [ "$LAG2" -gt "2500" ]; then
        echo "$FAIL Secondary DNS: $DNS2 @ $LAG2 msec. If consistently slow, try $AUTODNS (rly laggy)"
    elif [ "$LAG2" -gt "1000" ]; then
        echo "$WARN Secondary DNS: $DNS2 @ $LAG2 msec (laggy)"
    elif [ "$LAG2" -lt "1000" ]; then
        echo "$PASS Secondary DNS: $DNS2 @ $LAG2 msec"
    fi
else
    echo "$WARN Secondary DNS: None present, try $AUTODNS"
fi
if [ "$VCHK" = "true" ]; then
    DNSCACHE=`/home/spyware/code/firmware/current/bin/config_read dns_cache 2> /dev/null | grep dns_cache | awk '{ print $7 }'`
    DNSLOC=`/home/spyware/code/firmware/current/bin/config_read dns_force_local 2> /dev/null | grep dns_force_local | awk '{ print $7 }'`
    echo "$INFO DNS Caching: $DNSCACHE"
    echo "$INFO Force local DNS: $DNSLOC"
fi
echo

# Disconnected network and/or proxy
echo "PROXY CHECK"
PROXYCOUNT=`netstat -aptun | grep tcp | grep $IPADDR:8080 | wc -l | awk '{print $1}'`
mii-tool 2> /dev/null | grep "no link" > /dev/null
if [ "$?" = "0" ]; then IFDISCON=true ; fi
if [ "$PROXYCOUNT" -ge "3" ] && [ "$IFDISCON" = "true" ]; then
    echo "$INFO Unit's prolly a proxy"
fi
if [ "$PROXYCOUNT" -lt "3" ] && [ "$IFDISCON" != "true" ]; then
    echo "$INFO Unit's prolly inline"
fi
if [ "$PROXYCOUNT" -lt "3" ] && [ "$IFDISCON" = "true" ]; then
    echo "$INFO Unit has disconnected interface (Proxy Mode?)"
fi
echo

# FTP check
echo "FTP CHECK"
if [ "$FIRMWMAJ" -lt "40" ]; then
    FTPSIZE=`config_read %spy_ftp_exceptions_oversize% 2> /dev/null | grep spy_ftp_exceptions_oversize | awk '{print $5}'`
fi
if [ "$FIRMWMAJ" -ge "40" ]; then
    FTPSIZE=`config_read %spy_ftp_exceptions_oversize% 2> /dev/null | grep spy_ftp_exceptions_oversize | awk '{print $7}'`
fi
if [ "$FTPSIZE" = "-1" ]; then
    echo "$INFO FTP scanning disabled"
fi
echo

# Hardware
echo "HARDWARE"
# RAID
RSTAT=`/home/emailswitch/code/firmware/current/bin/raid_status.pl 2> /dev/null`
if [ "$MODEL" -ge "410" ]; then
    if [ "$RSTAT" = "RAID status OK" ]; then
      echo "$PASS $RSTAT"
    elif [ "$RSTAT" = "RAID unit \"[1-9]\" Degraded" ]; then
      echo "$FAIL $RSTAT"
    else
      echo "$WARN RAID status unknown. Check it yourself"
    fi
else
    echo "$INFO Model $MODEL does not use RAID"
fi

## Disk usage and friends
DP=`df -h | awk '{print $5}' | grep -v Use | awk -F% '{print $1}' | wc -l`
until [ $DP = 0 ]; do
    PART=`df -h | awk '{print $1}' | grep -v Filesystem | head -n $DP | tail -n 1`
    DU=`df -h | awk '{print $5}' | grep -v Use | awk -F% '{print $1}' | head -n $DP | tail -n 1`
    MNT=`df -h | awk '{print $6}' | grep -v Mounted | head -n $DP | tail -1`

    LIMIT=75
    if [ "$DU" -gt $LIMIT ]; then
        echo "$WARN $PART is above $LIMIT% ($DU%)!"
        OVER=1
    else OVER=0
    fi
    let DP-=1
done
if [ $OVER = 1 ]; then
    echo "$WARN One or more partitions over $LIMIT% usage"
else
    echo "$PASS Disk usage seems OK"
fi

## CPU use
if [ "$VCHK" = "true" ]; then 
    MAXUSED=`ps aux|awk 'NR > 0 { s +=$3 }; END {print s}'`
        echo "$INFO CPU usage: $MAXUSED%"
    CPULA=`cat /proc/loadavg 2> /dev/null |awk '{print $1, $2, $3}'`
        echo "$INFO CPU Load Avg. 1/5/15: $CPULA"
fi

# Memory use
if [ "$VCHK" = "true" ]; then
    MAXMEM=`top -b -n 1 | grep "Mem:" | awk '{print $2}' | awk -Fk '{print $1 / 1024}' | awk -F. '{print $1}'`
    MEMUSE=`ps aux|awk 'NR > 0 { s +=$4 }; END {print s}'`
        echo "$INFO $MAXMEM MB RAM installed"
        echo "$INFO RAM usage: $MEMUSE%"
fi
echo

# Authentication
echo "AUTHENTICATION"
    if [ "$FIRMWMAJ" -lt "40" ]; then
        LDAPSERVS=( `config_read %ldap_server_address% 2> /dev/null | grep ldap_server_address | awk '{print $5}' | xargs echo` )
    fi
    if [ "$FIRMWMAJ" -ge "40" ]; then
        LDAPSERVS=( `config_read %ldap_server_address% 2> /dev/null | grep ldap_server_address | awk '{print $7}' | xargs echo` )
    fi

    if [ "$LDAPSERVS" = "" ]; then
        echo "$INFO No LDAP servers configured"
        LDAPUSE=false
    else
        LDAPPORT=`config_read %ldap_server_port% 2> /dev/null | grep ldap_server_port | awk '{print $7}' | xargs echo`
        IN1=${#LDAPSERVS[@]}
        until [ $IN1 = 0 ]; do
            let IN1-=1
            echo quit | telnet ${LDAPSERVS[IN1]} $LDAPPORT 2> /dev/null | grep "Connected to" > /dev/null
            if [ $? != 0 ]; then
                echo "$FAIL Unable to connect to ${LDAPSERVS[IN1]} on port $LDAPPORT"
            else
                echo "$PASS Connected to port $LDAPPORT on ${LDAPSERVS[IN1]}"
            fi
        done
    fi
    if [ "$FIRMWMAJ" -lt "40" ]; then
        DCSERVS=( `config_read %sm_dc_ip% 2> /dev/null | grep sm_dc_ip | awk '{print $5}' | xargs echo` )
    fi
    if [ "$FIRMWMAJ" -ge "40" ]; then
        DCSERVS=( `config_read %sm_dc_ip% 2> /dev/null | grep sm_dc_ip | awk '{print $7}' | xargs echo` )
    fi
    if [ "$DCSERVS" = "" ] && [ "$LDAPUSE" != "false" ]; then
        echo "$WARN Using LDAP, no DC Agent configured"
    else
        DCPORT=5049
        IN1=${#DCSERVS[@]}
        until [ $IN1 = 0 ]; do
            let IN1-=1
            echo quit | telnet ${DCSERVS[IN1]} $DCPORT 2> /dev/null | grep "Connected to" > /dev/null
            if [ $? != 0 ]; then
                echo "$WARN Unable to connect to ${DCSERVS[IN1]} on port $DCPORT"
            else
                echo "$PASS Connected to port $DCPORT on ${DCSERVS[IN1]}"
            fi
        done
    fi

# NTLM
    NTLMADDR=`config_read %ntlm_server_address% 2> /dev/null | grep ntlm_server_address | awk '{print $5}'`
    NTLMHOST=`config_read %ntlm_server_hostname% 2> /dev/null | grep ntlm_server_hostname | awk '{print $5}'`
    if [ "$NTLMADDR" != "" ] && [ "$NTLMHOST" != "" ]; then
        echo "$INFO NTLM Configured"
        NTLMUSE=true
    fi
    if [ "$NTLMUSE" = "true" ] && [ "$LDAPUSE" != "false" ]; then
        echo "$WARN Using LDAP/NTLM Hybrid Auth. Citrix may not like this"
    fi
echo

# Logs
echo "LOG CHECK"
LOGERR="0"
LOGLIST=( /var/log/* /var/log/kernel/* /mail/log/daemons/* /mail/log/idmgr.log /tmp/web_error_log /tmp/clamd.log /mail/mysql/$HOSTNAME.err )
CHKLIST=( error fail warning eth hda )
INT=${#LOGLIST[@]}
until [ $INT = 0 ]; do
    let INT-=1
    WRD=${#CHKLIST[@]}
    LOGCHK=`file ${LOGLIST[INT]} | grep data | grep -v gzip | grep -v acpid | grep -v lastlog | grep -v wtmp | grep -v user.log`
    if [ "$LOGCHK" != "" ]; then
        echo "$WARN Possible log corruption in ${LOGLIST[INT]}"
        LOGERR="1"
    fi
        until [ $WRD = 0 ]; do
            let WRD-=1

            MAXCOUNT=1000
            COUNT=`cat ${LOGLIST[INT]} 2> /dev/null | grep -i ${CHKLIST[WRD]} | wc -l`
            if [ $COUNT -gt $MAXCOUNT ]; then
               if [ $LOGERR -eq "0" ]; then
                   echo
               fi
               echo "$WARN \"${CHKLIST[WRD]}\" appears in ${LOGLIST[INT]} more than $MAXCOUNT times!"
               LOGERR="1"
            fi
        done
done
if [ $LOGERR -eq "0" ]; then
    echo "$PASS No errors in logs"
fi
echo

# Processes
echo "PROCESS CHECK"
if [ "$FIRMWMAJ" -eq "40" ]; then
    PROCLIST=( apache clamd hemi-server hemi-logger iptmgr log_monitor logman pcaptor postmaster squid sysmon )
fi
if [ "$FIRMWMAJ" -gt "40" ]; then
    PROCLIST=( apache clamd hemi-server iptmgr log_monitor logman squid sysmon dnscache dnswatch )
fi
INT=${#PROCLIST[@]}
until [ $INT = 0 ]; do
    let INT-=1
    CURPROC=${PROCLIST[INT]}
    ps -A | grep $CURPROC > /dev/null
    if [ $? != 0 ]; then
        if [ "$OPMODE" = "AUDIT" ] && [ "$CURPROC" = "squid" ]; then
            echo "$WARN Squid down cuz it's in audit"
        else
            echo "$FAIL Do a service $CURPROC restart"
        fi
        WFDOWN=true
    fi
done
if [ "$WFDOWN" != "true" ]; then
   echo "$PASS All processes appear to be running"
fi
if [ "$VCHK" = "true" ]; then
    APACHESEM=`ipcs -s |grep nobody|wc -l |awk '{print $1}'`
    echo "$INFO Apache Semaphores: $APACHESEM"
fi
echo

# All done
echo "YFDIAG V2 COMPLETE"