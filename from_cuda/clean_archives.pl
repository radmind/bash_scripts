#!/usr/bin/perl -I/home/spyware/code/firmware/current/lib/perl5/site_perl/5.8.8/i686-linux-thread-multi
use strict;
use warnings;
use File::Find;
use PerlIO::gzip;
use File::Path;
use File::Copy;
my $archive_folder = "/mail/log/archive/";
my $garbage_file = "/mail/tmp/garbage";
if(! $ARGV[0]){$ARGV[0] = $archive_folder;}
my $total = 0;
sub convert($){
	my $suffix = ".tmp";
	my $file = shift;
	my $tempfile = $file . $suffix;
	open(IF, "<:gzip", $file);
	open(OF, ">:gzip", $tempfile);
	open(GF, ">>", $garbage_file);
	my $count = 0;
	while(<IF>){
		chomp;
		my @line = split('\s+',$_);
		if($#line == 17 || $#line == 29 || $#line >= 30){
			print OF "$_\n";
		}else{
			print GF "$_\n";
			$count++;
			next;
		}
	}
	close IF;
	close OF;
	close GF;
	print "$file\nstripped $count lines\n\n";
	move($tempfile, $file);
	$total += $count;
}
sub wanted{
	-f && /^cudaproxy\.log\.([0-9]){10}\.gz$/ or return;
	my $logfile = $File::Find::name;
	convert($logfile);
}
find(\&wanted, @ARGV);
print "Stripped a total of $total lines, examine $garbage_file for output\n";
