#!/usr/bin/perl

use strict;

##################################################
#Ver .1                                          #
#Script used to bulk check domains               #
#James Yu                                        #
##################################################

#Variables to invoke for count and array
my @domains;    # and array to hold the domains
my $count;      # variable object to place hold count for loops
my $response;   # to init variable for response
my $i;          #counter variable

print "Please enter the [Path] and/or [Filename] where your domains are listed:\n";
$response = <>;
chomp ($response);

system("clear");

if (-e $response)
{
        open FILE, "$response" or die "Cannot open file: $!\n";
        @domains = <FILE>; # test to see if the file will input
        close FILE;

        foreach (@domains)
        {
                #print @domains[$i];
                system ("/home/spyware/code/firmware/current/bin/hemi-client domaincat lookup $_");
                if ($_ == "nomatch")
                {
                        print @domains[$i];
                }
                $i++;
                print "\n";
        }
}

else { print "File does not exist: $response\n" }

