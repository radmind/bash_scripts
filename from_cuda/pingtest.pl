#!/usr/bin/perl -w

# iteration #5 to-do list:
# -print each return in text file
# -try to calculate MOS
#
# http://www.pingtest.net/learn.php
# http://technet.microsoft.com/en-us/library/bb894481%28v=office.12%29.aspx

use strict;

use Net::Ping;
use Time::HiRes;

use constant VERNUM            => 20120901004;

my $host;
my $proto;

print "HOST:\n";
$host = <>;
chomp ($host);

print "PROTOCOL:\n";
$proto = <>;
chomp ($proto);
print "\n"; 

sub STDDEV {
  my $ret;
  my $dur;
  my $ip;
  my $count;
  my $bytes;
  my $x;
  my $n;
  my $psum;
  my $psos;
  my $pvar;
  my $psd;
  my $pavg;

  my @bytes = qw(8 12 16 32 64 128 256 512 768 1024);
  my @pvals;
   
  open (PT, ">", "/tmp/netbench.txt") or die $!;
  foreach $bytes (@bytes) {     
    for ($count = 20; $count >= 1; $count--) {
      my $p = Net::Ping->new($proto, "3", $bytes);
      $p->hires();
      ($ret, $dur, $ip) = $p->ping($host);
      printf PT ("Size: $bytes - Time: %.2f ms\n", 1000 * $dur);
      push(@pvals, (1000 * $dur));
      foreach $x (@pvals) {
        $psum += $x;
        $n++;
        $psos += $x * $x;
      }
      $p->close();
    }
      $pvar = ($psos-(($psum * $psum)/$n))/($n-1);
      $psd = sqrt($pvar);
      $pavg = ($psum/$n);
      printf PT ("Size: $bytes - Average: $pavg - Deviance: $psd\n");                 
      printf ("Size: $bytes - Average: $pavg - Deviance: $psd\n");
  }      
  close (PT);
}

STDDEV();