#!/bin/bash 

if [ -w /home/remote/conns.txt ] 
	then echo -en "Local IPs \n"
else	
	echo "Cannot write to logfile, exiting now."
	exit 1
fi




/bin/netstat -an | /bin/grep tcp | /bin/egrep -v "0.0.0.0|LISTEN" | /bin/awk '{print $4}' | /bin/awk -F":" '{a[$1]++}END{for (i in a) print i, "\t" a[i]}' | /bin/sort -n | /bin/cat -n >> /home/remote/conns.txt
echo ""
echo ""
echo "Foreign IPs"
echo ""
/bin/netstat -an | /bin/grep tcp | /bin/grep -v 'LISTEN' | /bin/grep -v '0.0.0.0' | /bin/awk '{print $5}' | /bin/awk -F":" '{a[$1]++}END{for (i in a) print i, "\t" a[i]}' | /bin/sort -n | /bin/cat -n >> /home/remote/conns.txt
echo ""
echo ""
echo "Established or time waiting"
echo ""
/bin/netstat -an|/bin/grep -i 'estab\|time' >> /home/remote/conns.txt
echo ""
echo ""
echo "Non-web ports"
echo ""
/usr/sbin/tcpdump -c 300 -n port not 80 and port not 8080 and port not 8081 and port not 3128 and port not 3129 and port not 8000 and port not 22 and port not 443 and port not 5049 and port not 53 and port not 137 and port not 138 | /bin/cut -d' ' -f 5 | /bin/cut -d':' -f 1 | /bin/grep '^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*$' | /bin/cut -d'.' -f 5 | /bin/sort -r | /usr/bin/uniq -c |/bin/sort -n  >> /home/remote/conns.txt
echo ""
/bin/cat /home/remote/conns.txt
