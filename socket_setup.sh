#!/bin/bash
# socketsetup  - remote access behind NAT
# version .15
#
#

#local variables
pids=$( ps faux | grep -i a2cyber | grep -v grep | awk '{ print $2 }' )
npids=$( echo "$pids" | wc -w )

if [ "$npids" -ne 0 ]


then 
	if [ "$npids" -ge "3" ]
	then 
	kill -9 ` ps faux | grep -i a2cyber | grep -v grep | awk '{ print $2 }' `
		if [ "$?" -eq 0 ]
		then
			echo -en "we killed these pids: $pids....."
		else
			echo -en "We couldn't kill the pids... unclean exit"
			exit 1
		fi	
	fi


	if [ "$npids" -eq "2" ]
	then
		echo -en "\nSSH session is alive. Clean run `date`.\n"
		exit 0
	fi



else
	#establish a new ssh session to the bastion host
      screen -dm ssh -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes  -R 9001:localhost:22 alice@mysshbastion.com
	nnpids=$( ps faux | grep -i a2cyber | grep -v grep | awk '{ print $2 }' | wc -w )	
	if [ "$nnpids" -eq 2 ]	
		then	
			echo -en "We established a remote socket on the bastion. `date`"	
		else	
			echo -en "Something went wrong, there are "$nnpids" ssh connections running to the bastion.. `date`"
	fi
fi
