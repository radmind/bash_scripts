#!/bin/bash
#
# socket and packet capture analysis
#
#
# Boilerplate definitions for all scripts.
#
set -u		    # Undefined variables are latent bugs
TOOL=${0//*\//}     # Get the simple name of this script
TOOL_ARGS="$@"      # Save args in case other things call 'set'
host="$HOSTNAME"
# Turn on debugging if set. If not set or set to anything other
# than "echo", set it to ":".
[[ "echo" != "${DBG:=${DBG_DEFAULT:-:}}" ]] && DBG=":"
#
#
#script local vars
pcaplength="300"
tmppcap="/tmp/pcap1"

echo -en "Sufficient privledges are usually requred to run /usr/bin/tcpdump on a NIC\n"


echo -en "Begin traffic analysis...\n\n"
sleep +1
echo -en "Socket Statistics on $host:\n\n"

ss -nut | tail +2 | awk ' { print $6 } '

echo -en "\n\n"
sleep +1
echo -en "Initiating tcpdump...\n"
#add NIC selection...
#begin tcpdump

tcpdump -c "$pcaplength" -nn not multicast and not broadcast 2>&1  | cat >> "$tmppcap"

pcapstat=$( cat "$tmppcap" | cut -d' ' -f 5 | cut -d ':' -f 1 |/bin/grep '^[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*.*$' | sort -r | uniq  )
cat "$tmppcap" | grep -i permission
echo -en "found remote connections:\n\n"
echo "$pcapstat"



rm -rf "$tmppcap"
